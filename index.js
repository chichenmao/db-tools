const { dbInit, connectionStatus } = require('./src/db')
const { getInitAction } = require('./src/questions')
const { message } = require('./src/utils')

let answerProcessor

const init = async () => {
  if (Object.keys(connectionStatus).length === 0) {
    message('Checking DB connection status ...', 'info')
    await dbInit()
  }

  const answers = await getInitAction()
  if (!answerProcessor) {
    answerProcessor = require('./src/answerProcessor')
  }
  
  answerProcessor(answers)
}

exports.goTopMenu = init
init()