# DB-TOOLS

Command Gui Tools to do db operation, like add a lock. Since the tools site can access in baize. This tools is a supplement for tools site.

## Usage

1.  `npm ci`
2.  create `src/config/index.js` set your DB config like below.

    ```javascript
    module.exports = {
      DB: {
        LOCAL: 'mongodb://127.0.0.1:27017/august',
        DEV: '',
        STAGE: '',
        PROD: '',
      },
    };
    ```

    **Notice:** _Before run the script, please make sure your db is running._

3.  `node index.js`

## Functions

- Add a lock from production to local/dev/stage.
- Every action will write a log in `log` dir

## Demo

0. **Generate and search UUID**

1. **Add a lock to local env**

   ![](./doc/img/addLock.gif)

2. **List brand urls with brand**

3. **Modify brand urls with brand and urlId**

## TODOS

- [ ] list brand url
- [ ] update brand url
- [ ] add lock firmware
- [ ] modify lock firmware
- [x] write log
