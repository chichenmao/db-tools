const uuid = require('uuid')

/**
 * const OAuthClientSchema = new MongooseClient.Schema(
  {
    _id: {
      type: String,
      default: uuidv4(),
    },
    secret: {
      type: String,
      default: augustRandom.generateRandomString(32, 'printableasciichars'),
    },
    apiKey: String,
    name: String,
    icon: String,
    redirectUrls: [String],
    createdAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    strict: 'throw',
  }
);
*/

const oauthClient = {
  _id: uuid.v4(),
  secret: uuid.v4(),
  apiKey: uuid.v4(),
  name: "baidu",
  icon: "",
  redirectUrls: [],
  createdAt: Date.now()
}

console.log(JSON.stringify(oauthClient))