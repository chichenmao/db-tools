const { clients } = require('../../src/db')
const { ENV } = require('../../src/constants')
const questions = require('../../src/questions')
const { searchUUIDFlow } = require('../../src/flow/toolsFlow')

jest.mock('../../src/questions')

const testClient = clients[ENV.TEST]
const prodClient = clients[ENV.PROD]

const testCollection = testClient.model('devices.capabilities', null, { collName: 'devices.capabilities' })
const prodCollection = prodClient.model('devices.capabilities', null, { collName: 'devices.capabilities' })

describe('Test search uuid flow', () => {
  let totalCounts = 0
  let capType = null

  beforeAll(async () => {
    const docCount = await testCollection
      .countDocuments()
      .exec()

    if (docCount) {
      await testClient.dropCollection('devices.capabilities')
    }
  })

  beforeAll(async () => {
    const documents = await prodCollection.find().exec()
    totalCounts = documents.filter(item => !!item.universalDeviceID).length
  })

  beforeAll(async () => {
    const document = await prodCollection.findOne().exec()
    const { capabilities: { type } } = document
    capType = type
  })

  test('Capabilities type all in prod find data', async () => {
    questions.getCapabilitiesType.mockResolvedValue(null)
    questions.getEnv.mockResolvedValue(ENV.PROD)

    const capabilities = await searchUUIDFlow()

    expect(capabilities.length).toBe(totalCounts)
  })

  test('Capabilities type in prod find data', async () => {
    questions.getCapabilitiesType.mockResolvedValue(capType)
    questions.getEnv.mockResolvedValue(ENV.PROD)

    const capabilities = await searchUUIDFlow()

    expect(capabilities.length).toBe(1)
  })

  test('Capabilities type not in prod find data', async () => {
    questions.getCapabilitiesType.mockResolvedValue(1111)
    questions.getEnv.mockResolvedValue(ENV.PROD)

    const capabilities = await searchUUIDFlow()

    expect(capabilities.length).toBe(0)
  })

  afterEach(() => {
    questions.getCapabilitiesType.mockClear()
    questions.getEnv.mockClear()
  })
})

afterAll(async () => {
  await prodClient.disconnect()
  await testClient.disconnect()
})