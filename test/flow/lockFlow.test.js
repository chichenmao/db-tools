const { clients } = require('../../src/db')
const { ENV } = require('../../src/constants')
const questions = require('../../src/questions')
const { addLockFlow } = require('../../src/flow/lockFlow')

jest.mock('../../src/questions')

const testClient = clients[ENV.TEST]
const prodClient = clients[ENV.PROD]

const testCollection = testClient.model('lockmanufacturinginfo', null, { collName: 'lockmanufacturinginfo' })
const prodCollection = prodClient.model('lockmanufacturinginfo', null, { collName: 'lockmanufacturinginfo' })

let prodLock = null

describe('Test add lock flow', () => {
  beforeAll(async () => {
    const docCount = await testCollection
      .countDocuments()
      .exec()

    if (docCount) {
      await testClient.dropCollection('lockmanufacturinginfo')
    }
  })

  beforeAll(async () => {
    prodLock = await prodCollection.findOne().exec()
  })

  test('Lock not in test DB, insert success', async () => {
    const { serialNumber } = prodLock

    questions.getLock.mockResolvedValue(serialNumber)
    questions.getEnv.mockResolvedValue(ENV.TEST)

    const lock = await testCollection.findOne({ serialNumber }).exec()
    expect(lock).toBeNull()

    await addLockFlow()

    const insertedLock = await testCollection.findOne({ serialNumber })
    expect(insertedLock.serialNumber).toBe(serialNumber)
  })

  test('Lock existed in test DB, not insert', async () => {
    const { serialNumber } = prodLock

    questions.getLock.mockResolvedValue(serialNumber)
    questions.getEnv.mockResolvedValue(ENV.TEST)

    const lock = await testCollection.findOne({ serialNumber }).exec()
    expect(lock).not.toBeNull()

    await addLockFlow()

    const insertedLock = await testCollection.findOne({ serialNumber })
    expect(insertedLock.serialNumber).toBe(serialNumber)
  })

  afterEach(() => {
    questions.getLock.mockClear()
    questions.getEnv.mockClear()
  })
})

afterAll(async () => {
  await prodClient.disconnect()
  await testClient.disconnect()
})