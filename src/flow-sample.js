const runFlows = async (processors) => {
  const processorsLen = processors.length
  let context = {}
  const dispatch = async (i = 0) => {
    if (i >= processorsLen) return Promise.resolve()
    try {
      const processor = processors[i]
      return await processor(context, dispatch.bind(null, i + 1))
    } catch (err) {
      return Promise.reject(err)
    }
  }

  return await dispatch()
}

const a = async (ctx, next) => {
  console.log('a before next')
  await next()
  console.log('a after next')
}

const b = async (ctx, next) => {
  console.log('b before next')
  await next()
  console.log('b after next')
}
const c = async (ctx, next) => {
  console.log('c before next')
  await next()
  console.log('c after next')
}

runFlows([a, b, c])