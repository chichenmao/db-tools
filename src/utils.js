const chalk = require('chalk')
const Table = require('cli-table3')

exports.message = (message, type = "info") => {
  const chalkColor = {
    info: chalk.greenBright,
    bold: chalk.bold.whiteBright,
    success: chalk.greenBright,
    warning: chalk.yellowBright,
    error: chalk.redBright
  }

  const chalkMessage = chalkColor[type] || chalkColor.info
  const display = chalkMessage(message)
  console.log(display)
  return message
}

exports.consoleTable = ({ header = [], contents = [] }) => {
  const table = new Table({
    head: header,
    style: {
      head: ['bold', 'green']
    }
  })

  contents.forEach(content => table.push(content))
  const display = table.toString()
  console.log(display)
  return display
}

exports.getType = (value) => {
  const rawType = Object.prototype.toString.call(value).toLowerCase()
  const valueType = rawType.slice(8).replace(']', '')
  return valueType
}