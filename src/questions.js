const inquirer = require('inquirer')
const semver = require('semver')
const {
  QUESTION,
  ENV,
  PLATFORM,
  BRAND
} = require('./constants')

const { message } = require('./utils')
const REG = {
  PHONE: /^\+?\d+/,
  EMAIL: /.+@.+/
}

const envQuestion = {
  type: 'list',
  name: QUESTION.CHOOSE_ENV,
  message: 'Which environment ?',
  choices: Object.values(ENV)
}

exports.getEnv = async () => {
  const envValue = await inquirer.prompt([envQuestion])
  return envValue[QUESTION.CHOOSE_ENV]
}

const platformQuestion = {
  type: 'list',
  name: QUESTION.CHOOSE_PLATFORM,
  message: 'Which platform do you prefer ?',
  choices: [...Object.values(PLATFORM), 'both']
}

exports.getPlatform = async () => {
  const platformValue = await inquirer.prompt([platformQuestion])
  return platformValue[QUESTION.CHOOSE_PLATFORM]
}

const brandQuestion = {
  type: 'list',
  name: QUESTION.CHOOSE_BRAND,
  message: 'Which brand do you want ?',
  choices: Object.values(BRAND)
}

exports.getBrand = async () => {
  const brandValue = await inquirer.prompt([brandQuestion])
  return brandValue[QUESTION.CHOOSE_BRAND]
}

const versionQuestion = {
  type: 'input',
  name: QUESTION.INPUT_VERSION,
  message: 'Input version',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input version', 'bold'))
      return
    }

    if (!semver.valid(input)) {
      done(message('Please input correct version', 'bold'))
      return
    }

    done(null, true)
  }
}

exports.getVersion = async () => {
  const versionValue = await inquirer.prompt([versionQuestion])
  return versionValue[QUESTION.INPUT_VERSION]
}

const userIdQuestion = {
  type: 'input',
  name: QUESTION.INPUT_USER_ID,
  message: 'Input userID ("default" as default): ',
  default: 'default',
}

exports.getUserID = async () => {
  const userIdInput = await inquirer.prompt([userIdQuestion])
  return userIdInput[QUESTION.INPUT_USER_ID]
}

const operatorQuestion = {
  type: 'input',
  name: QUESTION.OPERATOR_NAME,
  message: `Input operator name（"default" is ${process.env.USER}):`,
  default: process.env.USER
}

exports.getOperator = async () => {
  const operatorInput = await inquirer.prompt([operatorQuestion])
  return operatorInput[QUESTION.OPERATOR_NAME]
}

const assignerQuestion = {
  type: 'input',
  name: QUESTION.ASSIGNER,
  message: 'Input assigner name',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input assigner', 'bold'))
      return
    }

    done(null, true)
  }
}

exports.getAssigner = async () => {
  const assignerInput = await inquirer.prompt([assignerQuestion])
  return assignerInput[QUESTION.ASSIGNER]
}

const getLockQuestion = {
  type: 'input',
  name: QUESTION.ADD_LOCK,
  message: 'Input serial number or lock ID: ',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input serial number or lock ID', 'bold'))
      return
    }
    done(null, true)
  }
}

exports.getLock = async () => {
  const lockInput = await inquirer.prompt([getLockQuestion])
  return lockInput[QUESTION.ADD_LOCK]
}

const getBridgeQuestion = {
  type: 'input',
  name: QUESTION.INPUT_BRIDGE_SN,
  message: 'Input bridge serial number',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input bridge serial number', 'bold'))
      return
    }
    done(null, true)
  }
}

exports.getBridge = async () => {
  const bridgeInput = await inquirer.prompt([getBridgeQuestion])
  return bridgeInput[QUESTION.INPUT_BRIDGE_SN]
}

const urlIDQuestion = {
  type: 'input',
  name: QUESTION.INPUT_URL_ID,
  message: 'Input urlID (Null is list all)',
}

exports.getUrlID = async () => {
  const urlValue = await inquirer.prompt([urlIDQuestion])
  return urlValue[QUESTION.INPUT_URL_ID]
}

const urlDownload = {
  type: 'input',
  name: QUESTION.ANDROID_DOWNLOAD_URL,
  message: 'Input download url for andorid'
}

exports.getAndroidDownloadUrl = async () => {
  const urlValue = await inquirer.prompt([urlDownload])
  return urlValue[QUESTION.ANDROID_DOWNLOAD_URL]
}

const capabilitiesTypeQuestion = {
  type: 'input',
  name: QUESTION.LIST_OR_SEARCH_UUID,
  message: 'Search uuid by capabilities.type (Null is list all)',
  default: null,
}

const generateUUDIQuestion = {
  type: 'input',
  name: QUESTION.GENERATE_UUID,
  message: 'Input lock type to generate a new UUID',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input lock type', 'bold'))
      return
    }

    if (!/\d{4}/.test(input)) {
      done(message('Please input lock type. Example 2201', 'bold'))
      return
    }
    done(null, true)
  }
}

exports.getCapabilitiesType = async (type = 'list') => {
  if (type === 'list') {
    const typeInput = await inquirer.prompt([capabilitiesTypeQuestion])
    return typeInput[QUESTION.LIST_OR_SEARCH_UUID]
  }

  const typeInput = await inquirer.prompt([generateUUDIQuestion])
  return typeInput[QUESTION.GENERATE_UUID]
}

const hackTokenQuestion = {
  type: 'input',
  name: QUESTION.HACK_USER,
  message: 'Input phone or email which you want to hack',
  validate: function (input) {
    const done = this.async()
    if (!input) {
      done(message('Please input phone or email', 'bold'))
      return
    }

    if (!REG.PHONE.test(input) && !REG.EMAIL.test(input)) {
      done(message('Input phone or email is invalid', 'bold'))
      return
    }

    done(null, true)
  }
}

exports.getHackInfo = async () => {
  const inputInfo = await inquirer.prompt([hackTokenQuestion])
  return inputInfo[QUESTION.HACK_USER]
}

const initQuestion = {
  type: 'list',
  name: QUESTION.ACTIONS,
  message: 'What do you want to do ?',
  choices: [
    QUESTION.SEARCH_LOCK_MANUFACTURE_INFO,
    QUESTION.ADD_LOCK,
    QUESTION.CHECK_LOCK_BINDER,
    QUESTION.LIST_OR_SEARCH_BRAND_URLS,
    QUESTION.UPDATE_ANDROID_DOWNLOAD_URL,
    QUESTION.SEARCH_BRIDGE_CONFIG,
    QUESTION.UPDATE_BRIDGE_CONFIG,
    QUESTION.GENERATE_CAPABILITIES_PAGE,
    QUESTION.HACK_USER,
    QUESTION.EXIT_APP
  ]
}

exports.getInitAction = async () => {
  const actions = await inquirer.prompt([initQuestion])
  return actions[QUESTION.ACTIONS]
}