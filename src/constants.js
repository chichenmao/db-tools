exports.ENV = {
  TEST: 'test',
  LOCAL: 'local',
  DEV: 'dev',
  STAGE: 'stage',
  PROD: 'prod'
}

exports.PLATFORM = {
  IOS: 'ios',
  ANDROID: 'andorid'
}

exports.STATUS = {
  SUCCESS: 'success',
  FAILED: 'failed'
}

exports.BRAND = {
  AUGUST: 'august',
  YALE: 'yale',
  YALECHINA: 'yalechina',
  PANPAN: 'panpan'
}

exports.CONFLUENCE_PAGE = {
  ANDROID_DOWNLOAD_URL: 'App download url for android'
}

exports.DEVICE_SERIAL = {
  M2: 'm2',
  L8: 'l8',
  M3: 'm3',
  Q1: 'q1',
}

exports.QUESTION = {
  ACTIONS: 'actions',
  ADD_LOCK: 'Add a lock to local/dev/stage',
  ANDROID_DOWNLOAD_URL: 'Download url for andorid',
  ASSIGNER: 'Input assigner name',
  CHOOSE_ENV: 'Choose environment',
  CHOOSE_PLATFORM: 'Choose platform',
  CHOOSE_BRAND: 'Choose brand',
  CHECK_LOCK_BINDER: 'Check who bind the lock',
  EXIT_APP: 'Exit',
  GENERATE_UUID: 'Generate UUID',
  GENERATE_CAPABILITIES_PAGE: 'Generate devices capabilities page',
  HACK_USER: 'Hack user',
  INPUT_VERSION: 'Input version',
  INPUT_URL_ID: 'Input urlID',
  INPUT_USER_ID: 'Input user id',
  INPUT_BRIDGE_SN: 'Input bridge serial number',
  LIST_OR_SEARCH_UUID: 'List or search UUID',
  LIST_OR_SEARCH_BRAND_URLS: 'List or search brand urls',
  OPERATOR_NAME: 'Input operator name',
  SEARCH_LOCK_MANUFACTURE_INFO: 'Search lock manufacturer info by SN/LockID',
  SEARCH_BRIDGE_CONFIG: 'Search bridge config (production environment)',
  UPDATE_ANDROID_DOWNLOAD_URL: 'Update Android download url',
  UPDATE_BRIDGE_CONFIG: 'Update bridge config (production environment)',
}