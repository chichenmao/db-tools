const collName = 'brandurls'

exports.searhBrandurls = async (client, query) => {
  const collection = client.model
    (collName, null, { collName })

  const result = await collection
    .find(query)
    .select({
      _id: 0,
      brand: 0,
      countryCode: 0,
      languageCode: 0
    })
    .exec()
  return result
}

exports.updateAndroidDownloadUrl = async (client, { brand, url }) => {
  const collection = client.model(collName, null, { collName })

  const result = await collection.findOneAndUpdate({
    brand,
    urlID: 'android-download-url'
  }, { $set: { url } }, { upsert: true })
    .exec()

  return result
}
