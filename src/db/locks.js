const collName = 'lockmanufacturinginfo'
const collLockInfo = 'locks'
const collLockSuperusers = 'locksuperusers'

exports.searchLock = async (client, query) => {
  const lockmanufacturinginfo = client.model
    (collName, null, { collName })

  const result = await lockmanufacturinginfo
    .findOne(query, { projection: { _id: 0 } })
    .exec()

  return result
}

exports.addNewLock = async (client, data) => {
  const lockmanufacturinginfo = client.model
    (collName, null, { collName })

  const { result, ops } = await lockmanufacturinginfo.insertOne({
    ...data,
    createdAt: new Date(),
    updatedAt: new Date()
  }).exec()

  const [newLock] = ops || []
  const { ok: status } = result

  return { status, newLock }
}

exports.searchLockSuperUsers = async (client, query) => {
  const lockSuperusers = client.model(collLockSuperusers, null, { collName: collLockSuperusers })
  const result = await lockSuperusers
    .find(query, { _id: 0, UserID: 1 })
    .exec()

  return result
}