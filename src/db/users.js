const { consoleTable } = require('./../utils')

const collUseridentifiers = 'useridentifiers'
const collUsers = 'users'

exports.searchUserId = async (client, query) => {
  const useridentifiers = client.model
    (collUseridentifiers, null, { collName: collUseridentifiers })

  const result = await useridentifiers
    .findOne(query)
    .select({
      _id: 0,
      userId: 1,
    })
    .exec()

  return result
}

exports.getUserIdentify = async (client, query) => {
  const useridentifiers = client.model(collUseridentifiers, null, { collName: collUseridentifiers })
  const result = await useridentifiers
    .find(query, { projection: { _id: 0 } })
    .exec()

  return result
}

exports.getUserInfo = async (client, query) => {
  const users = client.model(collUsers, null, { collName: collUsers })
  const result = await users
    .find(query, { projection: { _id: 0 } })
    .exec()

  return result
}

exports.getUserInfoById = async (client, userId) => {
  const useridentifiers = client.model
    (collUseridentifiers, null, { collName: collUseridentifiers })

  const users = client.model
    (collUsers, null, { collName: collUsers })

  const [identifiers, userInfo] = await Promise.all([
    useridentifiers.find({ userId }).select({ _id: 0, _value: 1 }).exec(),
    users.findOne({ _id: userId }).select({ _id: 0, FirstName: 1, LastName: 1 }).exec()
  ])


  const { phone, email, rawphone, rawemail } = identifiers
    .filter(item => item._value.includes('phone') || item._value.includes('email'))
    .reduce((contact, info) => {
      const { _value: identify } = info
      let spliter = identify.includes('email') ? 'email' : 'phone'
      return {
        ...contact,
        [spliter]: identify.split(`${spliter}:`)[1] || null,
        [`raw${spliter}`]: identify
      }
    }, {})

  const { FirstName, LastName } = userInfo

  consoleTable({
    header: ['userId', 'userName', 'phone', 'email'],
    contents: [
      [userId, `${FirstName} ${LastName}`, phone, email]
    ]
  })

  return {
    userId,
    FirstName,
    LastName,
    rawphone,
    rawemail
  }
}