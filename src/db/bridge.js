const collName = 'devices.connect'

exports.searchBridgeConfig = async (client, query) => {
  const collection = client.model
    (collName, null, { collName })

  const result = await collection
    .findOne(query)
    .select({
      _id: 0,
    })
    .exec()

  return result
}

exports.updateBridgeConfig = async (client, { deviceID, iConfig }) => {
  const collection = client.model(collName, null, { collName })

  const result = await collection.findOneAndUpdate({
    deviceID
  }, {
    $set: {
      deviceID,
      config: iConfig,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  }, { upsert: true })
    .exec()

  return result
}
