const { consoleTable } = require('./../utils')

const collName = 'devices.capabilities'

exports.searchUUID = async (client, query) => {
  const collection = client.model
    (collName, null, { collName })

  const result = await collection
    .find(query)
    .select({
      _id: 0,
      universalDeviceID: 1,
      'capabilities.type': 1,
      'capabilities.serialNumber': 1
    })
    .exec()

  const capabilities = result
    .filter(item => !!item.universalDeviceID)

  if (capabilities && capabilities.length > 0) {
    consoleTable({
      header: ['uuid', 'lock type', 'lock category'],
      contents: capabilities.map(item => {
        const { universalDeviceID: uuid, capabilities: { type, serialNumber } } = item
        return [uuid, type, serialNumber]
      })
    })
  }

  return capabilities
}