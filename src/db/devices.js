const collName = 'devices.capabilities'
const hostLockRegex = /\d{3,}\-\d{5,}/

exports.getAllDevicesCapabilities = async (client) => {
  const devicesCapabilities = client.model
    (collName, null, { collName })

  const result = await devicesCapabilities
    .find({}, { projection: { _id: 0, updatedAt: 0 } })
    .exec()

  return result
}

exports.getModuleCapabilities = async (client) => {
  const capabilities = await client.model(collName, null, { collName })
  const result = await capabilities.find({
    deviceSerialPrefix: { $exists: true }
  }, { projection: { _id: 0 }, sort: { updatedAt: -1 } })
    .exec()

  return result
}

exports.getHostLockCapabilities = async (client) => {
  const capabilities = await client.model(collName, null, { collName })
  const result = await capabilities.find({
    deviceVersion: hostLockRegex
  }, { projection: { _id: 0 }, sort: { updatedAt: -1 } })
    .exec()

  return result
}