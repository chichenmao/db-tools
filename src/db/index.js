const Mongolass = require('mongolass')
const { DB } = require('../config');
const { ENV } = require('../constants')
const { consoleTable } = require('../utils')

let clients = Object.values(ENV).reduce((clients, env) => {
  clients[env] = client = new Mongolass(DB[env], {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })

  return clients
}, {})

let connectionStatus = {}

const checkConnectionStatus = async () => {
  const clientsInfo = Object.entries(clients)
  const connectionList = []

  for ([env, client] of clientsInfo) {
    try {
      await client.connect()
      connectionStatus[env] = true
      connectionList.push([env, 'connected'])
    } catch (err) {
      connectionStatus[env] = false
      connectionList.push([env, 'disconnected'])
    }
  }

  consoleTable({
    header: ['ENV', 'DB CONNECT STATUS'],
    contents: connectionList
  })
}

const dbInit = async () => {
  try {
    await checkConnectionStatus()
  } catch (err) {
    console.error('Error in db init')
  }
}

module.exports = {
  clients,
  connectionStatus,
  dbInit
}