const path = require('path')
const log4js = require('log4js')
const { message } = require('./utils')

const logTypes = [
  { name: 'lock' },
  { name: 'tools' },
  { name: 'brandurls' },
  { name: 'hackToken' }
]

const appenders = (() => {
  return logTypes.reduce((appenders, logType) => {
    return {
      ...appenders,
      [logType.name]: {
        type: 'dateFile',
        layout: { type: 'basic' },
        filename: path.resolve(__dirname, `./../log/${logType.name}`),
        pattern: 'yyyy-MM-dd.log',
        alwaysIncludePattern: true,
      }
    }
  }, {
    stdout: {
      type: 'stdout'
    }
  })
})()

const categories = (() => {
  return logTypes.reduce((categories, logType) => {
    return {
      ...categories,
      [logType.name]: {
        appenders: [`${logType.name}`],
        level: 'INFO'
      }
    }
  }, {
    default: {
      appenders: ['stdout'],
      level: 'INFO'
    },
  })
})()

const logger = log4js.configure({
  appenders,
  categories
})

const loggerMap = (() => {
  return logTypes.reduce((logMap, logType) => {
    return {
      ...logMap,
      [logType.name]: logger.getLogger(`${logType.name}`)
    }
  }, {})
})()

const logProcessor = ({ logType = '', message = '', type = 'info' }) => {
  const log = loggerMap[logType]
  log[type](message)
}

const logFlowStart = ({ logType = '', flowName = '' }) => {
  const logMessage = message(`**** START ${flowName.toUpperCase()} FLOW ***`, 'bold')

  loggerMap[logType].info(logMessage)
}

const logFlowEnd = ({ logType = '', flowName = '' }) => {
  const logMessage = message(`**** END ${flowName.toUpperCase()} FLOW ***`, 'bold')

  loggerMap[logType].info(logMessage)
}

const logWrapper = ({ logType = '', flowName = '' }) => (fn) => async (...args) => {
  logFlowStart({ logType, flowName })
  const fnResult = await fn(...args)
  logFlowEnd({ logType, flowName })
  return fnResult
}

module.exports = {
  logFlowStart,
  logFlowEnd,
  logProcessor,
  logWrapper
}
