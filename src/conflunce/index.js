const axios = require('axios')
const { CONFLUENCE } = require('../config')

const conflunceRequest = axios.create({
  baseURL: CONFLUENCE.baseUrl,
  headers: { 'Accept': 'application/json' },
  auth: {
    username: CONFLUENCE.username,
    password: CONFLUENCE.password
  }
})

const createContentPage = async ({ title, type = 'page', space, body, ancestors = null }) => {
  const response = await conflunceRequest.post(`/rest/api/content`, {
    title, type, space, body, ancestors
  }).catch(err => console.log(err))
  const { data } = response || {}
  return data
}

const getContentById = async (id) => {
  const response = await conflunceRequest.get(`/rest/api/content/${id}?expand=body.storage,version`)
    .catch(err => console.error(err))
  const { data } = response || {}
  const { id: pageId, title, version, body } = data
  return { id: pageId, title, version, body }
}

const updateContent = async (id, { version, title, type = 'page', body }) => {
  const response = await conflunceRequest.put(
    `/rest/api/content/${id}`,
    { version: { number: version }, title, type, body }
  ).catch(err => console.error(err))
  return response
}

module.exports = {
  createContentPage,
  getContentById,
  updateContent
}