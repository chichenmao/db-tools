const cheerio = require('cheerio')

exports.renderTitleBlock = (title) => {
  return `
    <div styled="margin-bottom: 15px;">
      <h2>${title}</h2>
    </div>
  `
}

exports.renderBrandBlock = (brand = '') => {
  return `
    <div id=${brand}>
      <h2>${brand}</h2>
    </div>
  `
}

exports.renderTableLayout = (headers = []) => {
  return `
    <table>
      <thead>
        <tr style="background: #fefefe;">
          ${headers.map(header => `<td style="font-weight: bold;">${header}</td>`).join('')}
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  `
}

exports.renderTableContent = (contents = []) => {
  return `
    ${contents.map(content => `
      <tr>
        ${content.map(data => `<td>${data}</td>`).join('')}
      </tr>
    `).join('')}
  `
}

exports.composeTable = (tableLayout, tableContent) => {
  const $ = cheerio.load(tableLayout, { xmlMode: true })
  $('tbody').append(tableContent)
  $('table').wrap('<div></div>')
  return $.html()
}

exports.composeBrandBlock = (brandBlock, tableLayout, tableContent) => {
  const $ = cheerio.load(brandBlock, { xmlMode: true })
  const $$ = cheerio.load(tableLayout, { xmlMode: true })
  $$('tbody').append(tableContent)
  $('div').append($$.html())

  return $.html()
}

exports.composeUpdateTableData = (content, { tableHeader = [], tableContent = [] }) => {
  const $ = cheerio.load(content, { xmlMode: true })
  const _tableContent = exports.renderTableContent(tableContent)
  const hasContentTable = $('table').length > 0

  if (hasContentTable) {
    $('table tbody').append(_tableContent)
    return $.html()
  } else {
    const tableLayout = exports.renderTableLayout(tableHeader)
    return exports.composeTable(tableLayout, _tableContent)
  }
}