const { ENV } = require('./../constants')

// Here is the config file for db and secret
// You need to modify it to your db connect and secret
// otherwise the script can't run
module.exports = {
  DB: {
    [ENV.TEST]: 'mongodb://yourhost/yourdb',
    [ENV.LOCAL]: 'mongodb://yourhost/yourdb',
    [ENV.DEV]: 'mongodb://yourhost/yourdb',
    [ENV.STAGE]: 'mongodb://yourhost/yourdb',
    [ENV.PROD]: 'mongodb://yourhost/yourdb'
  },
  SECRET: {
    [ENV.LOCAL]: 'yoursecet',
    [ENV.DEV]: 'yoursecet'
  },
  CONFLUENCE: {
    username: "confluence user name",
    password: "confluence api key",
    baseUrl: "https://assaabloyapac.atlassian.net/wiki",
    bot: {
      spaceId: "spaceId",
      indexId: 'space index',
      indexTitle: 'space title',
      lockChangeLogId: 'pageid'
    },
  }
}