const { goTopMenu } = require('./../index')
const { addLockFlow, searchLockFlow, checkLockBinderFlow } = require('./flow/lockFlow')
const { brandurlsListFlow, androidDownloadUpdateFlow } = require('./flow/brandurlsFlow')
const { searchBridgeConfigFlow, updateBridgeConfigFlow } = require('./flow/bridgeFlow')
const { generateCapabilitiesPageFlow } = require('./flow/devicesFlow')
const { hackTokenFlow } = require('./flow/hackTokenFlow')

const { QUESTION } = require('./constants')

const { message } = require('./utils')

const runFlows = async (processors) => {
  const processorsLen = processors.length
  const context = {}
  const dispatch = async (i = 0) => {
    if (i >= processorsLen) return Promise.resolve()
    try {
      const processor = processors[i]
      return await processor(context, dispatch.bind(null, i + 1))
    } catch (err) {
      return Promise.reject(err)
    }
  }

  return await dispatch()
}

const flowMap = {
  [QUESTION.SEARCH_LOCK_MANUFACTURE_INFO]: searchLockFlow,
  [QUESTION.CHECK_LOCK_BINDER]: checkLockBinderFlow,
  [QUESTION.ADD_LOCK]: addLockFlow,
  [QUESTION.SEARCH_BRIDGE_CONFIG]: searchBridgeConfigFlow,
  [QUESTION.LIST_OR_SEARCH_BRAND_URLS]: brandurlsListFlow,
  [QUESTION.UPDATE_BRIDGE_CONFIG]: updateBridgeConfigFlow,
  [QUESTION.UPDATE_ANDROID_DOWNLOAD_URL]: androidDownloadUpdateFlow,
  [QUESTION.GENERATE_CAPABILITIES_PAGE]: generateCapabilitiesPageFlow,
  [QUESTION.HACK_USER]: hackTokenFlow,
  [QUESTION.EXIT_APP]: [() => {
    message('Bye', 'success')
    process.exit(0)
  }]
}

const answerProcessor = async (answer) => {
  const processors = flowMap[answer]
  try {
    await runFlows(processors)
    goTopMenu()
  } catch (error) {
    console.log(error)
  }
}

module.exports = answerProcessor