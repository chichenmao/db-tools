const jwt = require('jwt-simple')
const { clients } = require('../db')
const { getEnv, getHackInfo } = require('../questions')
const { logWrapper } = require('../logger')
const { searchUserId, getUserInfoById } = require('./../db/users')
const { message } = require('../utils')
const { SECRET } = require('../config')
const { ENV } = require('../constants')

const logType = 'hackToken'
const template = {
  "installId": "",
  "applicationId": "",
  "userId": "",
  "vInstallId": true,
  "vPassword": true,
  "vEmail": true,
  "vPhone": true,
  "hasInstallId": true,
  "hasPassword": true,
  "hasEmail": true,
  "hasPhone": true,
  "isLockedOut": false,
  "captcha": "",
  "email": [],
  "phone": [],
  "expiresAt": "2999-01-01T00:00:00.478Z",
  "temporaryAccountCreationPasswordLink": null,
  "iat": +new Date(),
  "exp": +new Date(),
  "FirstName": "",
  "LastName": ""
}

const hackToken = async () => {
  message('NOTICE: Only support for local and baize', 'bold')
  const env = await getEnv()

  if ([ENV.STAGE, ENV.PROD].includes(env)) {
    message('Sorry only support hack local and baize user', 'bold')
  }

  const userInfo = await getHackInfo()

  const query = {
    _value: new RegExp(`${userInfo}`, 'i')
  }

  const { userId } = await searchUserId(clients[env], query)
  if (!userId) {
    message('Sorry the user doesn\'t exist', 'bold')
  }

  const { FirstName, LastName, rawphone, rawemail } = await getUserInfoById(clients[env], userId)

  const token = jwt.encode(
    {
      ...template,
      userId,
      FirstName,
      LastName,
      email: [rawemail],
      phone: [rawphone]
    },
    SECRET[env]
  )

  message('Token created', 'success')
  message(token)
}

module.exports = {
  hackTokenFlow: logWrapper({ logType, flowName: 'HACK TOKEN' })(hackToken)
}