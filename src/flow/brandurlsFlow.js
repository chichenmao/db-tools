const moment = require('moment')

const { CONFLUENCE } = require('../config')
const { clients, connectionStatus } = require('../db')
const { searhBrandurls, updateAndroidDownloadUrl } = require('../db/brandurls')
const { getUrlID, getAndroidDownloadUrl } = require('./../questions')
const { ENV } = require('../constants')
const { getContentById, updateContent } = require('../conflunce')
const { composeUpdateTableData } = require('../conflunce/template')
const { message } = require('../utils')
const { setEnv, setVersion, setBrand, setOperator, setAssigner, checkClientConnection } = require('./commonFlow')

const setUrlID = async (ctx, next) => {
  const urlID = await getUrlID()
  ctx.urlID = urlID
  return next()
}

const getBrandUrls = async (ctx, next) => {
  const { client, urlID, brand } = ctx
  const query = urlID ? { brand, urlID: new RegExp(urlID, 'i') } : { brand }

  const brandUrls = await searhBrandurls(client, query)
  ctx.brandUrls = brandUrls
  return next()
}

const displayBrandUrls = async (ctx) => {
  const { brandUrls, brand } = ctx
  if (brandUrls) {
    console.log(brandUrls)
  } else {
    return message(`No urlID matched in brand: ${brand}`, 'info')
  }
}

const setAndroidDownloadUrl = async (ctx, next) => {
  const url = await getAndroidDownloadUrl()
  ctx.url = url
  return next()
}

const checkClientsConnection = async (ctx, next) => {
  if (
    connectionStatus[ENV.LOCAL]
    && connectionStatus[ENV.DEV]
    && connectionStatus[ENV.STAGE]
    && connectionStatus[ENV.PROD]
  ) {
    return next()
  }

  return message('Clients connection not all connected')
}

const updateAndroidDownloadUrls = async (ctx, next) => {
  const { brand, url } = ctx
  await Promise.all([
    updateAndroidDownloadUrl(clients[ENV.LOCAL], { brand, url }),
    updateAndroidDownloadUrl(clients[ENV.DEV], { brand, url }),
    updateAndroidDownloadUrl(clients[ENV.STAGE], { brand, url }),
    updateAndroidDownloadUrl(clients[ENV.PROD], { brand, url }),
  ])

  return next()
}

const updateAndroidDownloadUrlOperation = async ({ brand, url, appVersion, operator, assigner, date }) => {
  const { id, title, version, body } = await getContentById(CONFLUENCE.bot.urlPage[brand])
  const { storage: { value: content } = {} } = body || {}

  const updateData = composeUpdateTableData(content, {
    tableHeader: ['Brand', 'URL', 'Version', 'Assigner', 'Operator', 'Date'],
    tableContent: [
      [brand, url, appVersion, assigner, operator, date]
    ],
  })

  const { status } = await updateContent(id, {
    title,
    version: version.number + 1,
    body: {
      storage: {
        value: updateData,
        representation: "storage"
      }
    }
  })

  if (status === 200) {
    message('Confluence page updated', 'success')
  }
}

const updateAndroidDownloadPages = async (ctx) => {
  const operateDate = moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
  const { brand, url, version, operator, assigner } = ctx
  await updateAndroidDownloadUrlOperation(
    { brand, url, appVersion: version, operator, assigner, date: operateDate }
  )
  message('Android url page has updated', 'success')
}

module.exports = {
  brandurlsListFlow: [
    setEnv,
    setBrand,
    checkClientConnection,
    setUrlID,
    getBrandUrls,
    displayBrandUrls
  ],
  androidDownloadUpdateFlow: [
    setBrand,
    setAndroidDownloadUrl,
    setVersion,
    checkClientsConnection,
    updateAndroidDownloadUrls,
    setOperator,
    setAssigner,
    updateAndroidDownloadPages
  ]
}