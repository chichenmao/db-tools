const moment = require('moment')
const { ENV } = require('../constants')
const { message } = require('../utils')
const { CONFLUENCE } = require('../config')
const { searchBridgeConfig, updateBridgeConfig } = require('../db/bridge')
const { getBridge } = require('./../questions')
const { setEnv, setOperator, setAssigner, checkProdConnection } = require('./commonFlow')
const { getContentById, updateContent } = require('../conflunce')
const { composeUpdateTableData } = require('../conflunce/template')

const iConfigTemplate = {
  [ENV.DEV]: {
    rbsServerURL: 'https://rbs-sticky-rlcn-dev.aaecosys.com',
    otaServerURL: 'http://august-connect-ota.aaecosys.com',
    otaImagePrefix: 'WK-',
    rebootPeriod: 21600,
    telemetry_timeout: 90,
    rbsStickyServerURL: 'https://rbs-sticky-rlcn-dev.aaecosys.com',
    hyperBridge: true
  },
  [ENV.STAGE]: {
    rbsServerURL: 'https://pinnedrbs-rlcn-stage-aws.aaecosys.com',
    otaServerURL: 'http://august-connect-ota.aaecosys.com',
    otaImagePrefix: 'WK-',
    rebootPeriod: 21600,
    telemetry_timeout: 90,
    rbsStickyServerURL: 'https://rbs-sticky-rlcn-stage.aaecosys.com',
    hyperBridge: true
  },
  [ENV.PROD]: {
    rbsServerURL: 'https://pinnedrbs-rlcn-prod-aws.aaecosys.com',
    otaServerURL: 'http://august-connect-ota.aaecosys.com',
    otaImagePrefix: '',
    rebootPeriod: 21600,
    telemetry_timeout: 90,
    rbsStickyServerURL: 'https://rbs-sticky-rlcn.aaecosys.com',
    hyperBridge: true
  }
}

const setBridge = async (ctx, next) => {
  const bridgeInput = await getBridge()
  ctx.bridgeInput = bridgeInput
  return next()
}

const getBridgeConfig = async (ctx, next) => {
  const { bridgeInput, prodClient: client } = ctx
  const bridgeInfo = await searchBridgeConfig(client, { deviceID: bridgeInput })
  ctx.bridgeInfo = bridgeInfo
  return next()
}

const displayBridgeConfig = async (ctx, next) => {
  const { bridgeInfo, bridgeInput } = ctx
  if (bridgeInfo) {
    console.log(bridgeInfo)
    return next()
  } else {
    return message(`Bridge ${bridgeInput} not existed in production environment`, 'error')
  }
}

const updateBridgeConfigToTargetEnv = async (ctx, next) => {
  const { env, prodClient: client, bridgeInput } = ctx
  const iConfig = iConfigTemplate[env]

  if (!iConfig) {
    return message(`Can not set iconfig for ${env}`, 'error')
  }
  message(`Update bridge ${bridgeInput} to ${env}`, 'info')
  await updateBridgeConfig(client, { deviceID: bridgeInput, iConfig })
  return next()
}

const updateBridgeConfigOperation = async ({ env, operator, assigner, bridge, operateDate }) => {
  const { id, title, version, body } = await getContentById(CONFLUENCE.bot.bridgeConfigPage)
  const { storage: { value: content } = {} } = body || {}

  const updateData = composeUpdateTableData(content, {
    tableHeader: [
      'SerialNumber', 'Env', 'Assigner', 'Operator', 'Date'
    ],
    tableContent: [
      [bridge, env, assigner, operator, operateDate]
    ]
  })

  const { status } = await updateContent(id, {
    title,
    version: version.number + 1,
    body: {
      storage: {
        value: updateData,
        representation: "storage"
      }
    }
  })

  if (status === 200) {
    message('Confluence page updated', 'success')
  }
}

const updateBridgeConfigPage = async (ctx) => {
  const { operator, assigner, bridgeInput, env } = ctx
  message('Start update conflunce page', 'info')
  await updateBridgeConfigOperation({
    env,
    operator,
    assigner,
    bridge: bridgeInput,
    operateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
  })
}

module.exports = {
  searchBridgeConfigFlow: [
    setBridge,
    checkProdConnection,
    getBridgeConfig,
    displayBridgeConfig
  ],
  updateBridgeConfigFlow: [
    setBridge,
    checkProdConnection,
    getBridgeConfig,
    displayBridgeConfig,
    setEnv,
    updateBridgeConfigToTargetEnv,
    setOperator,
    setAssigner,
    updateBridgeConfigPage
  ]
}