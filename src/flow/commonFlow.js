const { ENV } = require('../constants')
const { clients, connectionStatus } = require('../db')
const { getEnv, getBrand, getPlatform, getVersion, getOperator, getAssigner } = require('./../questions')

exports.setEnv = async (ctx, next) => {
  const env = await getEnv()
  ctx.env = env
  return next()
}

exports.setBrand = async (ctx, next) => {
  const brand = await getBrand()
  ctx.brand = brand
  return next()
}

exports.setPlatform = async (ctx, next) => {
  const platform = await getPlatform()
  ctx.platform = platform
  return next()
}

exports.setVersion = async (ctx, next) => {
  const version = await getVersion()
  ctx.version = version
  return next()
}

exports.setOperator = async (ctx, next) => {
  const operator = await getOperator()
  ctx.operator = operator
  return next()
}

exports.setAssigner = async (ctx, next) => {
  const assigner = await getAssigner()
  ctx.assigner = assigner
  return next()
}

exports.checkProdConnection = (ctx, next) => {
  if (connectionStatus[ENV.PROD]) {
    ctx.prodClient = clients[ENV.PROD]
    return next()
  }
  return message(`Clinet ${ENV.PROD} is not connected, please check the connection`, 'error')
}

exports.checkClientConnection = (ctx, next) => {
  const { env } = ctx
  if (connectionStatus[env]) {
    ctx.client = clients[env]
    return next()
  }
  return message(`Clinet ${env} is not connected, please check the connection`, 'error')
}