const moment = require('moment')

const { ENV } = require('../constants')
const { message } = require('../utils')
const { CONFLUENCE } = require('../config')
const { searchLock, addNewLock, searchLockSuperUsers } = require('../db/locks')
const { getUserInfo, getUserIdentify } = require('../db/users')
const { getLock } = require('./../questions')
const { setEnv, setOperator, setAssigner, checkProdConnection, checkClientConnection } = require('./commonFlow')
const { getContentById, updateContent } = require('../conflunce')
const { composeUpdateTableData } = require('../conflunce/template')

const setLockInput = async (ctx, next) => {
  const lockInput = await getLock()
  ctx.lockInput = lockInput
  return next()
}

const getLockInfo = async (ctx, next) => {
  const { client, lockInput } = ctx
  try {
    const lockInfo = await searchLock(client, {
      $or: [
        { serialNumber: lockInput },
        { LockID: lockInput }
      ]
    })

    ctx.lockInfo = lockInfo
    return next()
  } catch (err) {
    console.error(err)
  }
}

const displayLockInfo = (ctx, next) => {
  const { lockInfo, env, lockInput } = ctx
  if (lockInfo) {
    console.log(lockInfo)
  } else {
    message(`Lock: ${lockInput} is not existed in ${env}`, 'info')
  }
  return next()
}

const checkProdLockExist = async (ctx, next) => {
  const { lockInput, prodClient } = ctx
  const lockInfo = await searchLock(prodClient, {
    $or: [
      { serialNumber: lockInput },
      { LockID: lockInput }
    ]
  })

  if (lockInfo) {
    message(`Lock Info in production: ${lockInput}`, 'info')
    ctx.lockInfo = lockInfo
    return next()
  }
  return message(`Lock: ${lockInput} is not existed in production environment`, 'info')
}

const addLock = async (ctx, next) => {
  const { env, lockInfo, client, lockInput } = ctx

  if (env === ENV.PROD) {
    return message('Adding a lock to production is NOT ALLOWED', 'error')
  }

  try {
    await addNewLock(client, lockInfo)
    message(`Lock ${lockInput} has added to ${env} successfully`, 'info')
    return next()
  } catch (err) {
    console.log(err)
  }
}

const updateLockAddOperation = async ({ env, operator, assigner, lock, operateDate }) => {
  const { serialNumber, LockID } = lock
  const { id, title, version, body } = await getContentById(CONFLUENCE.bot.lockChangeLogId)
  const { storage: { value: content } = {} } = body || {}

  const updateData = composeUpdateTableData(content, {
    tableHeader: [
      'SerialNumber', 'LockID', 'Env', 'Assigner', 'Operator', 'Date'
    ],
    tableContent: [
      [serialNumber, LockID, env, assigner, operator, operateDate]
    ]
  })

  const { status } = await updateContent(id, {
    title,
    version: version.number + 1,
    body: {
      storage: {
        value: updateData,
        representation: "storage"
      }
    }
  })

  if (status === 200) {
    message('Confluence page updated', 'success')
  }
}

const updateLockAddPage = async (ctx) => {
  const { env, operator, assigner, lockInfo, lockInput } = ctx
  if ([ENV.TEST, ENV.LOCAL].includes(env)) {
    return message(`Adding Lock ${lockInput} to ${env} not update to confluence`, 'bold')
  }
  message('Start update conflunce page', 'info')
  await updateLockAddOperation({
    env,
    operator,
    assigner,
    lock: lockInfo,
    operateDate: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
  })
}

const getLockSuperUsers = async (ctx, next) => {
  const { lockInfo: { LockID }, client } = ctx
  const superUsers = await searchLockSuperUsers(client, { LockID: LockID })
  const userIdList = superUsers.map(superuser => superuser.UserID)
  ctx.superUsers = userIdList
  return next()
}

const searchUserIdentifies = async (ctx, next) => {
  const { superUsers, client } = ctx
  const userIdentifies = await Promise.all(superUsers.map(userId => getUserIdentify(client, { userId })))

  const userSet = userIdentifies.flat(Infinity).reduce((_userSet, identify) => {
    const { userId, _value } = identify
    if (_value.includes('phone')) {
      _userSet[userId] = { ..._userSet[userId], phone: _value, }
    } else if (_value.includes('email')) {
      _userSet[userId] = { ..._userSet[userId], email: _value }
    }
    return _userSet
  }, {})

  ctx.userSet = userSet
  return next()
}

const searchUserInfo = async (ctx, next) => {
  const { userSet, client } = ctx
  const userInfos = await Promise.all(Object.keys(userSet).map(userId => getUserInfo(client, { _id: userId })))

  userInfos.flat(Infinity).forEach(info => {
    const { FirstName, LastName, UserID } = info
    userSet[UserID] = {
      ...userSet[UserID],
      firstName: FirstName,
      lastName: LastName
    }
  })
  return next()
}

const displayLockBinders = async (ctx) => {
  const { userSet, lockInfo: { LockID, serialNumber } } = ctx
  const lockInfo = {
    lockID: LockID,
    serialNumber
  }

  console.table(lockInfo)
  console.table(userSet)
}

module.exports = {
  addLockFlow: [
    setLockInput,
    setEnv,
    checkClientConnection,
    getLockInfo,
    (ctx, next) => {
      const { lockInfo, lockInput, env } = ctx
      if (lockInfo) {
        return message(`Lock ${lockInput} is already existed in ${env}`, 'info')
      }
      message(`Lock ${lockInput} is NOT existed in ${env}`, 'bold')
      return next()
    },
    checkProdConnection,
    checkProdLockExist,
    displayLockInfo,
    addLock,
    setOperator,
    setAssigner,
    updateLockAddPage,
  ],
  searchLockFlow: [
    setLockInput,
    setEnv,
    checkClientConnection,
    getLockInfo,
    displayLockInfo
  ],
  checkLockBinderFlow: [
    setLockInput,
    setEnv,
    checkClientConnection,
    getLockInfo,
    getLockSuperUsers,
    searchUserIdentifies,
    searchUserInfo,
    displayLockBinders
  ]
}