const moment = require('moment')
const { CONFLUENCE } = require('../config')
const { DEVICE_SERIAL } = require('../constants')
const { getModuleCapabilities, getHostLockCapabilities } = require('../db/devices')
const { createContentPage } = require('../conflunce')
const { composeUpdateTableData, renderTitleBlock } = require('../conflunce/template')
const { setVersion, setEnv, checkClientConnection } = require('./commonFlow')
const { message, getType } = require('../utils')

const getDeviceSerial = (deviceSerialNumber) => {
  if (['323E', '315E', '268E', '268P'].includes(deviceSerialNumber)) return DEVICE_SERIAL.M3
  if (['SCL257', 'SCL258', 'SCL259'].includes(deviceSerialNumber)) return DEVICE_SERIAL.Q1
  if (['YMH70A'].includes(deviceSerialNumber)) return DEVICE_SERIAL.L8
  return DEVICE_SERIAL.M2
}

const getDevicesCapabilities = async (ctx, next) => {
  const { client } = ctx
  const [moduleList, hostLockList] = await Promise.all(
    [getModuleCapabilities(client), getHostLockCapabilities(client)]
  )

  ctx.moduleList = moduleList
  ctx.hostLockList = hostLockList
  return next()
}

const mergeFields = (capabilitiesList) => {
  const fieldSet = capabilitiesList.reduce((keySet, capability) => {
    Object.keys(capability).forEach(key => keySet.add(key))
    return keySet
  }, new Set())
  return [...fieldSet]
}

const categorizeFieldSet = (fieldList, capabilityList) => {
  const fieldTypeSet = fieldList.reduce((_fieldTypeSet, field) => {
    capabilityList.forEach(capability => {
      const fieldValue = capability[field]
      const fieldType = getType(fieldValue)

      if (fieldValue !== undefined && !_fieldTypeSet[fieldType].has(field)) {
        // Special case for masterPinSlot since this field is number but has value ''
        if (fieldType === 'string' && field === 'masterPinSlot') {
          _fieldTypeSet['number'].add('masterPinSlot')
        } else {
          _fieldTypeSet[fieldType].add(field)
        }
      }
    })

    return _fieldTypeSet
  }, {
    number: new Set(),
    string: new Set(),
    boolean: new Set(),
    array: new Set(),
    object: new Set()
  })

  return fieldTypeSet
}

const sortFieldSetToList = (fieldTypeSet, { order = ['number', 'string', 'boolean', 'array', 'object'] } = {}) => {
  return order.reduce((fieldList, orderType) => {
    const fieldSet = fieldTypeSet[orderType]
    if (fieldSet) {
      const setList = [...fieldSet]
      setList.sort()
      return [...fieldList, ...setList]
    }
    return fieldList
  }, [])
}

const transposeCapabilitiesList = (devicesCapabilitiesList) => {
  const { deviceNameList, capabilityList } = devicesCapabilitiesList.reduce((_devicesSet, device) => {
    const { capabilities } = device
    _devicesSet.deviceNameList.push(capabilities.serialNumber)
    _devicesSet.capabilityList.push(capabilities)
    return _devicesSet
  }, {
    deviceNameList: [],
    capabilityList: []
  })

  const mergedFields = mergeFields(capabilityList)
  const fieldTypeSet = categorizeFieldSet(mergedFields, capabilityList)
  const sortedFieldList = sortFieldSetToList(fieldTypeSet)

  const transposedList = sortedFieldList.reduce((list, field) => {
    const fieldList = capabilityList.map(capability => {
      const fieldValue = [undefined, null].includes(capability[field]) ? '' : capability[field]
      return typeof fieldValue === 'object' ? JSON.stringify(fieldValue) : fieldValue
    })

    return [...list, [field, ...fieldList]]
  }, [])

  return [deviceNameList, ...transposedList]
}

const categorizeHostLock = (hostLockList) => {
  return hostLockList.reduce((categorySet, hostLock) => {
    const { capabilities: { serialNumber } } = hostLock
    const serial = getDeviceSerial(serialNumber)
    categorySet[serial].push(hostLock)
    return categorySet
  }, {
    [DEVICE_SERIAL.M2]: [],
    [DEVICE_SERIAL.L8]: [],
    [DEVICE_SERIAL.M3]: [],
    [DEVICE_SERIAL.Q1]: []
  })
}

const transferModuleCapabilityData = async (ctx, next) => {
  const { moduleList } = ctx
  const [m2ModuleName, ...m2ModuleTransposedList] = transposeCapabilitiesList([
    moduleList.find(lock => lock.deviceSerialPrefix === DEVICE_SERIAL.M2)
  ])
  const [l8ModuleName, ...l8ModuleTransposedList] = transposeCapabilitiesList([
    moduleList.find(lock => lock.deviceSerialPrefix === DEVICE_SERIAL.L8)
  ])
  const [m3ModuleName, ...m3ModuleTransposedList] = transposeCapabilitiesList([
    moduleList.find(lock => lock.deviceSerialPrefix === DEVICE_SERIAL.M3)
  ])
  const [q1ModuleName, ...q1ModuleTransposedList] = transposeCapabilitiesList([
    moduleList.find(lock => lock.deviceSerialPrefix === DEVICE_SERIAL.Q1)
  ])

  ctx.moduleLockSet = {
    [DEVICE_SERIAL.M2]: { names: m2ModuleName, data: m2ModuleTransposedList },
    [DEVICE_SERIAL.L8]: { names: l8ModuleName, data: l8ModuleTransposedList },
    [DEVICE_SERIAL.M3]: { names: m3ModuleName, data: m3ModuleTransposedList },
    [DEVICE_SERIAL.Q1]: { names: q1ModuleName, data: q1ModuleTransposedList },
  }

  return next()
}

const transferHostLockCapabilityData = async (ctx, next) => {
  const { hostLockList } = ctx
  const {
    [DEVICE_SERIAL.M2]: m2Locks,
    [DEVICE_SERIAL.L8]: l8Locks,
    [DEVICE_SERIAL.M3]: m3Locks,
    [DEVICE_SERIAL.Q1]: q1Locks,
  } = categorizeHostLock(hostLockList)

  const [m2LockNames, ...m2TransposedList] = transposeCapabilitiesList(m2Locks)
  const [l8LockNames, ...l8TransposedList] = transposeCapabilitiesList(l8Locks)
  const [m3LockNames, ...m3TransposedList] = transposeCapabilitiesList(m3Locks)
  const [q1LockNames, ...q1TransposedList] = transposeCapabilitiesList(q1Locks)

  ctx.hostLockSet = {
    [DEVICE_SERIAL.M2]: { names: m2LockNames, data: m2TransposedList },
    [DEVICE_SERIAL.L8]: { names: l8LockNames, data: l8TransposedList },
    [DEVICE_SERIAL.M3]: { names: m3LockNames, data: m3TransposedList },
    [DEVICE_SERIAL.Q1]: { names: q1LockNames, data: q1TransposedList },
  }

  return next()
}

const generateLockData = (moduleLockSet, type = 'Module') => {
  return Object.values(DEVICE_SERIAL).map(serial => {
    const { names, data } = moduleLockSet[serial]
    const composedData = composeUpdateTableData('<div></div>', {
      tableHeader: ['Capability', ...names],
      tableContent: [
        ...data
      ],
    })

    return `${renderTitleBlock(`${type}: ${serial}`)}${composedData}<div style="margin-top: 20px;"></div>`
  }).join('<hr />')
}

const createCapabilitiesPage = async (ctx, next) => {
  const { version, moduleLockSet, hostLockSet } = ctx
  message(`Creating pages: Devices Capabilities - Release version: ${version} ...`, 'info')

  const moduleLockContent = generateLockData(moduleLockSet, 'Module Lock')
  const hostLockContent = generateLockData(hostLockSet, 'Host Lock')

  await Promise.all([
    createContentPage({
      title: `${version} - Module Capabilities`,
      space: { key: CONFLUENCE.bot.spaceKey },
      ancestors: [{ id: CONFLUENCE.bot.deviceCapabilityPageId }],
      body: {
        storage: {
          value: moduleLockContent,
          representation: "storage"
        },
      }
    }),
    createContentPage({
      title: `${version} - Host Lock Capabilities`,
      space: { key: CONFLUENCE.bot.spaceKey },
      ancestors: [{ id: CONFLUENCE.bot.deviceCapabilityPageId }],
      body: {
        storage: {
          value: hostLockContent,
          representation: "storage"
        },
      }
    })
  ])

  message(`Devices Capabilities - Release version: ${version} created successfully`, 'info')
  return next()
}

module.exports = {
  generateCapabilitiesPageFlow: [
    setEnv,
    setVersion,
    checkClientConnection,
    getDevicesCapabilities,
    transferModuleCapabilityData,
    transferHostLockCapabilityData,
    createCapabilitiesPage,
  ]
}