const shortId = require('shortid')

const { clients } = require('../db')
const { searchUUID } = require('../db/tools')
const { getEnv, getCapabilitiesType } = require('./../questions')
const { logWrapper } = require('../logger')

const logType = 'tools'

const searchUUIDFlow = async () => {
  const env = await getEnv()
  const capabilitiesType = await getCapabilitiesType()
  const query = capabilitiesType ?
    { 'capabilities.type': Number(capabilitiesType) } : {}
  const capabilities = await searchUUID(clients[env], query)
  return capabilities
}

const generateUUIDFlow = async () => {
  const env = await getEnv()
  const capabilitiesType = await getCapabilitiesType()
  const query = capabilitiesType ?
    { 'capabilities.type': Number(capabilitiesType) } : {}
  const capabilities = await searchUUID(clients[env], query)
  return capabilities
}

module.exports = {
  searchUUIDFlow: logWrapper({ logType, flowName: 'LIST OR SEARCH UUID' })(searchUUIDFlow)
}